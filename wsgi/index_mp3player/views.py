# -*- coding:utf-8 -*-
from flask import render_template, request
from tools.index_mp3_tools import get_mp3_list_li, get_mp3_list_td
from app import app

@app.route("/test/")
def index_player():
    mp3_list = get_mp3_list_li()
    if not mp3_list:
        mp3_list = get_mp3_list_td() 

    mp3_js_str_all = ""
    for mp3_index in mp3_list:
        mp3_js_str = "".join(('{"title":"', mp3_index[0], '","mp3":"', mp3_index[1], '"},'))
        mp3_js_str_all = "".join((mp3_js_str_all, mp3_js_str))
    mp3_js_str_all = mp3_js_str_all[:-1]
    #print(mp3_js_str_all)
    return render_template("index_mp3player.html", mp3_list=mp3_js_str_all)

@app.route("/play_index_mp3")
def play_index_mp3():
    url_mp3 = request.args.get("url_mp3")
    mp3_list = dict()
    mp3_js_str_all = ""
    if url_mp3:
        mp3_list = get_mp3_list_li(url_mp3)
        if not mp3_list:
            mp3_list = get_mp3_list_td(url_mp3)

        for mp3_index in mp3_list:
            mp3_js_str = "".join(('{"title":"', mp3_index[0], '","mp3":"', mp3_index[1], '"},'))
            mp3_js_str_all = "".join((mp3_js_str_all, mp3_js_str))
        mp3_js_str_all = mp3_js_str_all[:-1]

    return render_template("index_mp3player.html", mp3_list=mp3_js_str_all)

