#-*-coding:utf-8 -*-
#!/usr/bin/env python
import urllib2
from bs4 import BeautifulSoup


def get_mp3_list_li(url_str="http://www.belita.org/BELITAS%20INSTRUMENTAL/"):
    page = urllib2.urlopen(url_str).read()
    soup = BeautifulSoup(page, "html.parser")
    soup.prettify()
    mp3_list = list()
    for li_item in soup.findAll('li'):
        mp3_list.append((li_item.a.text, "".join((url_str, li_item.a["href"]))))

    return mp3_list

def get_mp3_list_td(url_str="http://hcmaslov.d-real.sci-nnov.ru/public/mp3/Beatles/01%20Please,%20Please%20Me/"):
    page = urllib2.urlopen(url_str).read()
    soup = BeautifulSoup(page, "html.parser")
    soup.prettify()
    mp3_list = list()
    for td_item in soup.findAll('td'):
        #mp3_list.append((li_item.a.text, "".join((url_str, li_item.a["href"]))))
        if td_item.a and (".mp3" in td_item.a["href"] or ".MP3" in td_item.a["href"]):
            mp3_list.append((td_item.a.text, "".join((url_str, td_item.a["href"]))))
            print(td_item.a)
    return mp3_list

if __name__ == "__main__" :
   print(get_mp3_list_td("http://hcmaslov.d-real.sci-nnov.ru/public/mp3/Metallica/"))
