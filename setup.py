from setuptools import setup

setup(name='smallstar',
      version='1.0',
      description='smallstar for openshift',
      author='Chen Yen-Chang',
      author_email='yenchang.chen@gmail.com',
      url='https://www.facebook.com/yenchang.chen.7',
      install_requires=['Flask', 'beautifulsoup4'],
     )
